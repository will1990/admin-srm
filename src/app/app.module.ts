import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { PainelComponent } from './painel/painel.component';
import { AtalhosComponent } from './atalhos/atalhos.component';
import { AlertasComponent } from './alertas/alertas.component';
import { SidebarComponent } from './sidebar/sidebar.component';

//import { FontAwesomeModule } from '@fortawesome/fontawesome-free';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PainelComponent,
    AtalhosComponent,
    AlertasComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
  //  FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
